<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ajudeamaju
 */
global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<!-- META -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content=""/>
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="" />

	<!-- TÍTULO -->
	<title>Ajude a Maju Blog</title>

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="http://abraceamaju.com.br/padrinho/wp-content/themes/ajudeamaju/img/favicon.png" />

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	<!-- TOPO -->
	<header class="topo">
		<div class="containerMenu">
			<div class="row">

				<!-- LOGO -->
				<div class="menu-topo">
					<div class="row">
						<div class="col-md-3">
							<div class="logo">
								<a href="<?php echo get_home_url(); ?>/blog">
									<img class="img-responsive" src="<?php echo $configuracao['header_logo']['url'] ?>" alt="Ajude a Maju">
								</a>
							</div>
						</div>
						<!-- MENU -->	
						<div class="col-md-9">
							<div class="navbar" role="navigation">	
								<!-- MENU MOBILE TRIGGER -->
								<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
									<span class="sr-only"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<!-- MENU MOBILE -->
								<div class="row navbar-header">			
									<nav class="collapse navbar-collapse" id="collapse">
										<?php 
										$menu = array(
											'theme_location'  => '',
											'menu'            => 'Menu Principal',
											'container'       => false,
											'container_class' => '',
											'container_id'    => '',
											'menu_class'      => 'nav navbar-nav',
											'menu_id'         => '',
											'echo'            => true,
											'fallback_cb'     => 'wp_page_menu',
											'before'          => '',
											'after'           => '',
											'link_before'     => '',
											'link_after'      => '',
											'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
											'depth'           => 2,
											'walker'          => ''
										);
										wp_nav_menu( $menu );
										?>
									</nav>		
									<!-- <div class="imagem-busca-lista"><a href="#" class="imagem-busca" title="Busca"><img src="<?php echo get_template_directory_uri(); ?>/img/busca.png" alt=""></a></div> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="caixaPesquisa">
		<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
			<input type="text" name="s" id="search" placeholder="Digite aqui sua pesquisa" autofocus="">
			<input type="submit" value="Pesquisar">
		</form>
	</div>
