$(function(){
    /*****************************************
    *           CARROSSEIS                   *
    *****************************************/
        //CARROSSEL DE DESTAQUE
        $("#bannerDestaqueInicial").owlCarousel({
            items : 1,
            dots: true,
            loop: false,
            lazyLoad: true,
            mouseDrag:true,
            touchDrag  : true,         
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            smartSpeed: 450,

            //CARROSSEL RESPONSIVO
            //responsiveClass:true,             
            /*responsive:{
                320:{
                    items:1
                },
                600:{
                    items:2
                },
               
                991:{
                    items:2
                },
                1024:{
                    items:3
                },
                1440:{
                    items:4
                },
                                        
            }*/                             
            
        });
        //BOTÕES DO CARROSSEL ESTANTE
        var carrossel_destaque = $("#bannerDestaqueInicial").data('owlCarousel');
        $('.carrosselDestaqueEsquerda').click(function(){ carrossel_destaque.prev(); });
        $('.carrosselDestaqueDireita').click(function(){ carrossel_destaque.next(); });


          $(".menuAtivar button").click(function(e){
            $("#navMenu").addClass("open");
        });
          $("#closeMenu").click(function(e){
            $("#navMenu").removeClass("open");
        });
          $("ul .menu-item a").click(function(){
            setTimeout(function(){
             $("#navMenu").removeClass("open");
            }, 1000);
        });

        var userFeed = new Instafeed({
            get: 'user',
            userId: '431966971',
            clientId: 'c7c6b34f76c04fd695e16fd6b5769c4f',
            accessToken: '431966971.1677ed0.41fb392cdc854aa3b717c612f19e0a46',
            resolution: 'standard_resolution',
            template: '<a href="{{link}}" target="_blank" id="{{id}}"><div class="itemInstagram" style="background:url({{image}})"><small class="likeComments"><span class="likes"><i class="fas fa-heart"></i> {{likes}}</span><span class="comments"> <i class="fas fa-comment"></i> {{comments}}</span></small></div></a>',
            sortBy: 'most-recent',
            limit: 8,
            links: false
        });
        userFeed.run();

        $('.menu-pesquisa').click(function(e){
            $('.navbar-collapse').removeClass('in');
            $('#search').focus();
            $('.caixaPesquisa').toggleClass('aberto');
            e.preventDefault();
        });

});
