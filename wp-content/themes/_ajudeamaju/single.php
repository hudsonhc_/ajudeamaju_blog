<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ajudeamaju
 */
	$titulo = get_the_title();
	$idCategoria;
	$categorias = get_the_category();
	foreach ($categorias as $categorias ) {
		$idCategoria = $categorias->cat_ID;
		$nomeCategoria = $categorias->name;
	}
	$linkCategoria = get_category_link($idCategoria);
	$imagemDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$imagemDestaque = $imagemDestaque[0];
get_header();
?>

	<div class="pg pg-post">
		<div class="containerFull">
			<div class="breadCrumbs">
				<ul>
					<li><a href="<?php echo get_home_url(); ?>/blog">Portal da Maju</a></li>
					<li><a href="<?php echo $linkCategoria ?>"><?php echo $nomeCategoria; ?></a></li>
					<li class="ativo"><a href=""><?php echo $titulo; ?></a></li>
				</ul>
			</div>

			<div class="row">
				<div class="col-md-9">
					<div class="conteudoPost">
						<h1 class="tituloPost"><?php echo $titulo; ?></h1>
						<a href="<?php echo $linkCategoria ?>" class="categoriaPost"><?php echo $nomeCategoria; ?></a>
						<span class="dataPost"><?php echo get_the_date('d/m/Y '); ?></span>
						<figure class="imagemDestaquePost" style="background: url(<?php echo $imagemDestaque ?>);">
							<img src="<?php echo $imagemDestaque ?>" alt="<?php echo $titulo; ?>">
						</figure>
						<div class="textoPost">
							<?php //echo apply_filters('the_content',$post->post_content); 
								while(have_posts()){
									the_post();
									echo the_content();
								}
							?>
						</div>
						<div class="disqus"></div>
					</div>
				</div>
				<div class="col-md-3">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</div>

<?php
get_footer();
