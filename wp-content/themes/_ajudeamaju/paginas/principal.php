<?php
/**
 * Template Name: Principal
 * Description: Página Principal
 *
 * @package ajudeamaju
 */
global $configuracao;
get_header(); ?>
	<!-- PÁGINA PRINCIPAL -->
	<div class="pg pg-principal">
		<div class="containerFull">
			<div class="menuAtivar">
				Menu
				<button class="openMenu">
					<span></span>
					<span></span>
					<span></span>
				</button>
			</div>
			<nav id="navMenu">
				<button id="closeMenu">X</button>
				<!-- <a href="">Portal</a>
				<a href="">Seja um padrinho</a>
				<a href="">Portal da Maju</a> -->
				<?php 
					$menu = array(
						'theme_location'  => '',
						'menu'            => 'Menu Página Principal',
						'container'       => false,
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => '',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 2,
						'walker'          => ''
					);
					wp_nav_menu( $menu );
					?>
			</nav>
		</div>

		<section class="areaConteudo">
			<div class="containerFull">
				<div class="row">
					<div class="logo">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logoMaju.png" alt="">
					</div>
					<div class="areaTexto">
						<article>
							<h6><?php echo $configuracao['pg_principal_quem_e_titulo']; ?></h6>
							<p><?php echo $configuracao['pg_principal_quem_e_texto']; ?></p>

							<h6><?php echo $configuracao['pg_principal_como_ajudar_titulo']; ?></h6>
							<p><?php echo $configuracao['pg_principal_como_ajudar_titulo_texto']; ?></p>
						</article>

						<div class="plataformas">
							<ul>
								<li>
									<a href="<?php echo $configuracao['pg_principal_links_lojinha']; ?>">
										<h3>Conheça a lojinha</h3>
										<img src="<?php echo get_template_directory_uri(); ?>/img/lojaa.png" alt="">
										<span class="fas fa-stroopwafel"></span>
									</a>
								</li>
								<li>
									<a href="<?php echo $configuracao['pg_principal_links_padrinhos']; ?>">
										<h3>Quero Ser Padrinho!</h3>
										<img src="<?php echo get_template_directory_uri(); ?>/img/padrinhos.png" alt="">
										<span class="fas fa-stroopwafel"></span>
									</a>
								</li>
								<li>
									<a href="<?php echo $configuracao['pg_principal_links_portal']; ?>">
										<h3>Portal da Maju</h3>
										<img src="<?php echo get_template_directory_uri(); ?>/img/portal.png" alt="">
										<span class="fas fa-stroopwafel"></span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>