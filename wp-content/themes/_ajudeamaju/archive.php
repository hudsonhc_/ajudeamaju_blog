<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ajudeamaju
 */
	$categoriaAtual = get_queried_object();
	
	$separaItem    = explode("|", get_queried_object()->description);
	$corCategoria  = $separaItem[0];
	$iconeCategoria= $separaItem[1];
	if(!$corCategoria){
		$corCategoria = "#44E7CC";
	}

	if(!$iconeCategoria){
		$iconeCategoria = get_template_directory_uri() . '/img/iconeCategory.png';
	}

	$bannerCategoria = z_taxonomy_image_url(get_queried_object()->cat_ID);
	
	if($bannerCategoria == false || $bannerCategoria == ''){
		$bannerCategoria = get_template_directory_uri() . '/img/bannerCategoria.png';
	}
get_header();
?>

	<div class="pg pg-categoria">
		<div class="nomeCategoria" style="background-image: url(<?php echo $bannerCategoria; ?>);">
			<h3 style="color: <?php echo $corCategoria; ?>"><?php echo get_queried_object()->name ?></h3>
			<span class="bordaCategoria" style="background: <?php echo $corCategoria; ?>;"></span>
		</div>
		<div class="containerFull">
			<div class="postsCategoria">
				<ul class="listaDePosts">
					<?php  
						while(have_posts()):
							the_post();
							$link = get_permalink();
							$titulo = get_the_title();

							$imagemDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$imagemDestaque = $imagemDestaque[0];
					?>
					<li class="postCategoria">
						<article class="informacoesPost">
							<a href="<?php echo $link ?>" class="linkPost">
								<h3 class="tituloPost"><?php echo $titulo ?></h3>
								<p class="descricaoPost"><?php customExcerpt(200); ?></p>
							</a>
								<span class="dataPost"><?php echo get_the_date('d/m/Y '); ?></span>
								<div class="compartilhamentoPost">
									<ul>
										<li><a href="#"><i class="fab fa-twitter"></i></a></li>
										<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									</ul>
								</div>
							<a href="<?php echo $link; ?>">
								<figure class="imagemDestaquePost">
									<img src="<?php echo $imagemDestaque ?>" alt="#">
								</figure>
							</a>
							<a href="<?php echo $link;?>" class="linkFora"><i class="fas fa-chevron-right"></i></a>
						</article>
					</li>
				<?php endwhile; wp_reset_query(); ?>
				</ul>
				<a href="#" class="verMais">Ver mais</a>
			</div>
		</div>
	</div>

<?php
get_footer();
