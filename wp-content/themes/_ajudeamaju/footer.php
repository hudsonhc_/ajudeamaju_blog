<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ajudeamaju
 */
global $configuracao;
?>
<script src="https://matthewelsom.com/assets/js/libs/instafeed.min.js"></script>
<footer>
	<div class="containerFull">
		<div class="rodape">
			<div class="row">
				<div class="col-sm-6">
					<div class="logo">
						<a href="<?php echo get_home_url(); ?>/blog">
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/img/logofooter.png" alt="Ajude a Maju">
							</figure>
						</a>
						<p>PORTAL DA MAJU</p>
					</div>
					<div class="texto">
						<p><?php echo $configuracao['rodape_descricao']; ?></p>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="menuRodape">
						<?php 
							$menu = array(
								'theme_location'  => '',
								'menu'            => 'Menu Principal',
								'container'       => false,
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => '',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 2,
								'walker'          => ''
							);
							wp_nav_menu( $menu );
							?>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
