<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ajudeamaju
 */
global $post;
get_header();
?>
	<div class="pg pg-inicial">
		<section class="bannerDestaque">
			<button class="carrosselDestaqueEsquerda"></button>
			<button class="carrosselDestaqueDireita"></button>
			<?php 
				//LOOP DE DESTAQUES
				$postsDestaques = new WP_Query(array(
					'post_type'     => 'destaque',
					'posts_per_page'   => -1,
					)
				);
			?>
			<h6 class="hidden">Banner Destaque</h6>
			<div class="bannerDestaqueInicial" id="bannerDestaqueInicial">
			<?php while($postsDestaques->have_posts()): 
				$postsDestaques->the_post();
				global $post;
				//FOTO DESTACADA
				$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoPost = $fotoPost[0];

			?>
				<div class="item">
					<a href="#">
						<figure>
							<img src="<?php echo $fotoPost; ?>" alt="<?php echo get_the_title(); ?>">
						</figure>
					</a>
				</div>
			<?php endwhile; wp_reset_query(); ?>
			</div>
		</section>
		<img class="marcador" src="<?php echo get_template_directory_uri(); ?>/img/afterinicial.png" alt="Marcador">
		<div class="containerFull">
			<div class="row">
				<div class="col-md-9">
					<section class="postsInicial">

						<?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" button_label="Ver Mais"]'); ?>
						<!-- <div class="ver-mais">
							<button>VER MAIS</button>
						</div> -->
					</section>
				</div>
				<div class="col-md-3">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();
