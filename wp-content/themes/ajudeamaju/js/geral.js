$(function(){
    /*****************************************
    *           CARROSSEIS                   *
    *****************************************/
        //CARROSSEL DE DESTAQUE
        $("#bannerDestaqueInicial").owlCarousel({
            items : 1,
            dots: true,
            loop: false,
            lazyLoad: true,
            mouseDrag:true,
            touchDrag  : true,         
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            smartSpeed: 450,

            //CARROSSEL RESPONSIVO
            //responsiveClass:true,             
            /*responsive:{
                320:{
                    items:1
                },
                600:{
                    items:2
                },
               
                991:{
                    items:2
                },
                1024:{
                    items:3
                },
                1440:{
                    items:4
                },
                                        
            }*/                             
            
        });
        //BOTÕES DO CARROSSEL ESTANTE
        var carrossel_destaque = $("#bannerDestaqueInicial").data('owlCarousel');
        $('.carrosselDestaqueEsquerda').click(function(){ carrossel_destaque.prev(); });
        $('.carrosselDestaqueDireita').click(function(){ carrossel_destaque.next(); });


          $(".menuAtivar button").click(function(e){
            $("#navMenu").addClass("open");
        });
          $("#closeMenu").click(function(e){
            $("#navMenu").removeClass("open");
        });
          $("ul .menu-item a").click(function(){
            setTimeout(function(){
             $("#navMenu").removeClass("open");
            }, 1000);
        });

        var userFeed = new Instafeed({
            get: 'user',
            userId: '7033113543',
            clientId: 'f9fd7e67d14848a5b99c0c1db8f0bcc9',
            accessToken: '7033113543.1677ed0.365e488d65214a78bc6fb288e6f9f264',
            resolution: 'standard_resolution',
            template: '<a href="{{link}}" target="_blank" id="{{id}}"><div class="itemInstagram" style="background:url({{image}})"><small class="likeComments"><span class="likes"><i class="fas fa-heart"></i> {{likes}}</span><span class="comments"> <i class="fas fa-comment"></i> {{comments}}</span></small></div></a>',
            sortBy: 'most-recent',
            limit: 9,
            links: false
        });
        userFeed.run();

        $('.menu-pesquisa').click(function(e){
            $('.navbar-collapse').removeClass('in');
            $('#search').focus();
            $('.caixaPesquisa').toggleClass('aberto');
            e.preventDefault();
        });

});
