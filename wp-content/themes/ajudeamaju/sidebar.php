<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ajudeamaju
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

	<div class="sidebar">
		<div class="sidebar-secoes">
			<section class="secao-mais-procurados">
				<h6 class="hidden">SEÇÃO POSTS MAIS PROCURADOS</h6>
				<h4>mais vistos</h4>
				<?php  
				while(have_posts()):
					the_post();

					$imagemDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$imagemDestaque = $imagemDestaque[0];
					?>
				<div class="noticia">
					<div class="titulo">
						<p><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></p>
						<span><?php echo get_the_date('d/m/Y '); ?></span>
					</div>
					<div class="imagem">
						<figure>
							<img src="<?php echo $imagemDestaque; ?>" alt="<?php echo get_the_title(); ?>">
						</figure>
					</div>
				</div>
			<?php endwhile; wp_reset_query(); ?>
				<span class="borda"></span>
			</section>
			<section class="secao-instagram">
				<h6 class="hidden">SEÇÃO INSTAGRAM</h6>
				<h4>siga @ajudeamaju no insta</h4>
				<span class="borda"></span>
				<div id="instafeed">

				</div>
			</section>
			<section class="secao-facebook">
				<h6 class="hidden">SEÇÃO FACEBOOK</h6>
				<div class="facebook">
					<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fajudeamaju%2F&tabs=timeline&width=390&height=540&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="390" height="540" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
				</div>
			</section>
		</div>
	</div>

