<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package ajudeamaju
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>
			
			
			<div class="pg pg-busca" style="display: ;">
				<div class="tituloResultado">
					<p><?php
				/* translators: %s: search query. */
				printf( esc_html__( 'Resultados para: %s', 'ajudeamaju' ), '<span>' . get_search_query() . '</span>' );
				?></p>
					
				</div>
				<div class="postsEncontrados">
					<div class="containerFull">
						<ul class="listaDePosts">
							<?php  
								while(have_posts()):
									the_post();

									$titulo = get_the_title();
									$link = get_permalink();
									$data = get_the_date('d/m/Y '); 
									$imagemDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$imagemDestaque = $imagemDestaque[0];
								?>
								<?php if($imagemDestaque): ?>
							<li class="postCategoria">
								<article class="informacoesPost">
									<a href="<?php echo $link; ?>" class="linkPost">
										<h3 class="tituloPost"><?php echo $titulo; ?></h3>
										<p class="descricaoPost"><?php customExcerpt(200); ?></p>
									</a>
										<span class="dataPost"><?php echo $data; ?></span>
										<div class="compartilhamentoPost">
											<ul>
												<li><a rel="nofollow" href="http://twitter.com/home?status=<?php the_title(); ?>+<?php the_permalink() ?>" target="_blank" title="Compartilhe esse post no Twitter"><i class="fab fa-twitter"></i></a></li>
												<li><a rel="nofollow" href="http://www.facebook.com/share.php?u=<?php the_permalink() ?>" title="Compartilhe esse post no Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
											</ul>
										</div>
									
									<a href="<?php echo $link ?>">
										<figure class="imagemDestaquePost">
											<img src="<?php echo $imagemDestaque ?>" alt="#">
										</figure>
									</a>
									<a href="<?php echo $link ?>" class="linkFora"><i class="fas fa-chevron-right"></i></a>
								</article>
									<!-- <a href="#" class="botaoVerPost"><i class="fas fa-chevron-right"></i></a> -->
							</li>

						<?php endif; endwhile; wp_reset_query(); ?>
						</ul>
					</div>
				</div>
			</div>
		<?php endif; ?>
			

<?php
get_footer();
