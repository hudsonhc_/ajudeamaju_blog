<?php			
$categorias = get_the_category();
$idCategoria;
								
foreach ($categorias as $categorias){
	$nomeCategoria = $categorias->name;
	$idCategoria   = $categorias->cat_ID;
	//$bannerCategoria     = z_taxonomy_image_url(get_queried_object()->cat_ID);
	$separaItem    = explode("|", $categorias->description);
	$corCategoria  = $separaItem[0];
	$iconeCategoria= $separaItem[1];
}
								
if(!$corCategoria){
	$corCategoria = "#44E7CC";
}
if(!$iconeCategoria){
	$iconeCategoria = get_template_directory_uri() . '/img/iconeCategory.png';
}

$imagemDestaquePost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$imagemDestaquePost = $imagemDestaquePost[0];
	$titulo = get_the_title();
	$link = get_permalink();

?>
<div class="post">
	<div class="row">
		<div class="texto">
        	<div class="titulo">
            	<a href="<?php echo $link; ?>">
                	<h2><?php echo $titulo; ?></h2>
            	</a>
        	</div>
        	<div class="conteudo">
            	<p><?php customExcerpt(200); ?></p>
        	</div>
        	<div class="categoria" style="border-bottom: 3px solid <?php echo $corCategoria; ?>;">
            <a href="<?php echo get_category_link( $idCategoria ) ?>" style="color: <?php echo $corCategoria; ?>;"><img src="<?php echo $iconeCategoria ?>" alt=""><?php echo $nomeCategoria; ?></a>
        	</div>
    	</div>
    	<div class="imagem">
        	<div class="data">
            	<span style="color: <?php echo $corCategoria; ?>;"><?php echo get_the_date('d/m/Y '); ?></span>
            <ul>
            	<li>
                	<img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png" alt="">
            	</li>
            	<li>
                	<img src="<?php echo get_template_directory_uri(); ?>/img/pinterest.png" alt="">
            	</li>
            	<li>
            		<img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="">
            	</li>
            	<li>
                	<img src="<?php echo get_template_directory_uri(); ?>/img/instagram.png" alt="">
            	</li>
            </ul>
        </div>
        <figure>
        	<img src="<?php echo $imagemDestaquePost ?>" alt="<?php echo $titulo; ?>">
        </figure>
        <a href="<?php echo $link; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/seta.png" alt="<?php echo $titulo; ?>" class="btn-entrar"></a>
    	</div>
	</div>
</div>